terraform {
  required_version = ">= 0.12.0"
}

provider "aws" {
  version = "~> 2.23"
  region  = "us-east-1"
}


# This `label` is needed to prevent `count can't be computed` errors
module "label" {
  source     = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=tags/0.2.1"
  namespace  = "${var.namespace}"
  name       = "${var.name}"
  stage      = "${var.stage}"
  delimiter  = "${var.delimiter}"
  attributes = "${var.attributes}"
  tags       = "${var.tags}"
  enabled    = "${var.enabled}"
}

# This `label` is needed to prevent `count can't be computed` errors
module "cluster_label" {
  source     = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=tags/0.2.1"
  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  delimiter  = "${var.delimiter}"
  attributes = "${compact(concat(var.attributes, list("cluster")))}"
  tags       = "${var.tags}"
  enabled    = "${var.enabled}"
}

locals {
  # The usage of the specific kubernetes.io/cluster/* resource tags below are required
  # for EKS and Kubernetes to discover and manage networking resources
  # https://www.terraform.io/docs/providers/aws/guides/eks-getting-started.html#base-vpc-networking
  tags = "${merge(var.tags, map("kubernetes.io/cluster/${module.label.id}", "shared"))}"
}

module "vpc" {
  source     = "git::https://github.com/cloudposse/terraform-aws-vpc.git?ref=tags/0.8.0"
  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = "${var.attributes}"
  tags       = "${local.tags}"
  cidr_block = "${var.vpc_cidr_block}"
}

module "subnets" {
  source              = "git::https://github.com/cloudposse/terraform-aws-dynamic-subnets.git?ref=tags/0.16.0"
  availability_zones  = "${var.availability_zones}"
  namespace           = "${var.namespace}"
  stage               = "${var.stage}"
  name                = "${var.name}"
  attributes          = "${var.attributes}"
  tags                = "${local.tags}"
  #region              = "${var.region}"
  vpc_id              = "${module.vpc.vpc_id}"
  igw_id              = "${module.vpc.igw_id}"
  cidr_block          = "${module.vpc.vpc_cidr_block}"
  nat_gateway_enabled = "true"
}
# ////////////////////////////////////////////////////////////////////////////////////////////
#   EKS CLUSTER
# ////////////////////////////////////////////////////////////////////////////////////////////
locals {
  cluster_name = "${var.name}-${var.environment}-eks"
  common_tags = map(
    # "Name", "${var.name}-${var.environment}",
    "Namespace", "${var.namespace}",
    "Application", "${var.name}",
    "Environment", "${var.environment}",
    "Team", "${var.team}",
    "Owner", "${var.owner}")
}


# EKS Master
resource "aws_iam_role" "eks_cluster" {
  name = "${var.name}-${var.environment}-eks-role"
  description = "Allow the EKS service to manage or retrieve data from other AWS services"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "eks_cluster-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks_cluster.name
}

resource "aws_iam_role_policy_attachment" "eks_cluster-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.eks_cluster.name
}

resource "aws_security_group" "eks_cluster" {
  name        = "${var.name}-${var.environment}-cluster"
  description = "Cluster communication with worker nodes"
  vpc_id      = "${module.vpc.vpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = "${merge(
    local.common_tags, 
    map("Name", "${var.name}-${var.environment}-eks-sg")
    )
  }"
}

resource "aws_eks_cluster" "main" {
  name     = local.cluster_name
  role_arn = aws_iam_role.eks_cluster.arn
  version  = var.eks_version

  vpc_config {
    security_group_ids = ["${aws_security_group.eks_cluster.id}"]
    subnet_ids         = "${module.subnets.private_subnet_ids}"
  }

  depends_on = [
    "aws_iam_role_policy_attachment.eks_cluster-AmazonEKSClusterPolicy",
    "aws_iam_role_policy_attachment.eks_cluster-AmazonEKSServicePolicy",
  ]
}

locals {
  kubeconfig = <<KUBECONFIG

apiVersion: v1
clusters:
- cluster:
    server: ${aws_eks_cluster.main.endpoint}
    certificate-authority-data: ${aws_eks_cluster.main.certificate_authority.0.data}
  name: ${local.cluster_name}
contexts:
- context:
    cluster: ${local.cluster_name}
    user: aws
  name: aws
current-context: aws
kind: Config
preferences: {}
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws-iam-authenticator
      args:
        - "token"
        - "-i"
        - "${local.cluster_name}"

KUBECONFIG
}

# Kubernetes Worker Nodes IAM and Instance Profile

resource "aws_iam_role" "eks-node" {
  name = "eks-node-gitlab-prd-role"
  tags = "${merge(
    local.common_tags, 
    map("Name", "${var.name}-${var.environment}-eks-node-role"),
    map("Description", "IAM role and policy to allow the worker nodes to manage or retrieve data from other AWS services. It is used by Kubernetes to allow worker nodes to join the cluster.")
    )
  }"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "eks-node-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = "${aws_iam_role.eks-node.name}"
}

resource "aws_iam_role_policy_attachment" "eks-node-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = "${aws_iam_role.eks-node.name}"
}

resource "aws_iam_role_policy_attachment" "eks-node-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = "${aws_iam_role.eks-node.name}"
}

# Custom permissions for inner AWS services
resource "aws_iam_policy" "worker_node_additional_permissions_policy" {
  name        = "${var.name}-${var.environment}-additional-permissions-policy"
  policy      = file("${path.module}/resources/worker_node_additional_permissions.json")
}

resource "aws_iam_role_policy_attachment" "worker_node_additional_permissions_attachment" {
  role       = "${aws_iam_role.eks-node.name}"
  policy_arn = "${aws_iam_policy.worker_node_additional_permissions_policy.arn}"
}

# Custom permissions for IAM
resource "aws_iam_policy" "iam_policy_additional_permissions_policy" {
  name        = "${var.name}-${var.environment}-additional-permissions-policy"
  policy      = file("${path.module}/resources/iam_policy.json")
}

resource "aws_iam_role_policy_attachment" "iam_policy_additional_permissions_attachment" {
  role       = "${aws_iam_role.eks-node.name}"
  policy_arn = "${aws_iam_policy.iam_policy_additional_permissions_policy.arn}"
}

resource "aws_iam_instance_profile" "eks-node" {
  name = "terraform-eks-prd"
  role = "${aws_iam_role.eks-node.name}"
}

# Worker node Security Groups

resource "aws_security_group" "eks-node" {
  name        = "${var.name}-${var.environment}-eks-node"
  description = "Security group for all nodes in the cluster"
  vpc_id      = "${module.vpc.vpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = "${merge(
    local.common_tags, 
    map("Name", "${var.name}-${var.environment}-eks-nodes")
    )
  }"
}

resource "aws_security_group_rule" "eks-node-ingress-self" {
  description              = "Allow node to communicate with each other"
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.eks-node.id}"
  source_security_group_id = "${aws_security_group.eks-node.id}"
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "eks-node-ingress-cluster" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.eks-node.id}"
  source_security_group_id = "${aws_security_group.eks_cluster.id}"
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "eks-node-nodeport-ingress-cluster" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 30036
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.eks-node.id}"
  cidr_blocks              = [ "${var.vpc_cidr_block}" ]
  to_port                  = 30036
  type                     = "ingress"
}

resource "aws_security_group_rule" "eks-cluster-ingress-node-https" {
  description              = "Allow pods to communicate with the cluster API Server"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.eks_cluster.id}"
  source_security_group_id = "${aws_security_group.eks-node.id}"
  to_port                  = 443
  type                     = "ingress"
}

# Auto Scaling Group

data "aws_ami" "eks-worker" {
  filter {
    name   = "name"
    values = ["amazon-eks-node-${aws_eks_cluster.main.version}-v*"]
  }

  most_recent = true
  owners      = ["602401143452"] # Amazon EKS AMI Account ID
}

locals {
  eks-node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.main.endpoint}' --b64-cluster-ca '${aws_eks_cluster.main.certificate_authority.0.data}' '${local.cluster_name}'
USERDATA
}

resource "aws_launch_configuration" "eks-nodes" {
  associate_public_ip_address = false
  iam_instance_profile        = "${aws_iam_instance_profile.eks-node.name}"
  image_id                    = "${data.aws_ami.eks-worker.id}"
  instance_type               = var.instance_type
  name_prefix                 = "${var.name}-${var.environment}-eks-worker"
  security_groups             = ["${aws_security_group.eks-node.id}"]
  user_data_base64            = "${base64encode(local.eks-node-userdata)}"
  key_name                    = "gitlab_xxx.pem"
   # root disk
  root_block_device {
    volume_size           = "200"
    volume_type           = "gp2"
  
  }
  
  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_autoscaling_group" "eks-nodes" {
  desired_capacity     = "${var.asg_desired_capacity}"
  launch_configuration = "${aws_launch_configuration.eks-nodes.id}"
  max_size             = "${var.asg_max_size}"
  min_size             = "${var.asg_min_size}"
  name                 = "${var.name}-${var.environment}-eks-asg"
  vpc_zone_identifier  = "${module.subnets.private_subnet_ids}"

  tag {
    key                 = "Name"
    value               = "${local.cluster_name}-node"
    propagate_at_launch = true
  }

  tag {
    key                 = "kubernetes.io/cluster/${local.cluster_name}"
    value               = "owned"
    propagate_at_launch = true
  }

  tag {
    key                 = "Environment"
    value               = "${var.environment}"
    propagate_at_launch = true
  }

    tag {
    key                 = "Team"
    value               = "${var.team}"
    propagate_at_launch = true
  }

    tag {
    key                 = "Owner"
    value               = "${var.owner}"
    propagate_at_launch = true
  }
}

locals {
  config_map_aws_auth = <<CONFIGMAPAWSAUTH

apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  mapRoles: |
    - rolearn: ${aws_iam_role.eks-node.arn}
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
CONFIGMAPAWSAUTH
}

# ////////////////////////////////////////////////////////////////////////////////////////////
#   RDS AURORA
# ////////////////////////////////////////////////////////////////////////////////////////////

module "db" {
  source                          = "terraform-aws-modules/rds-aurora/aws"
  version                         = "~> 2.0"
  name                            = "${var.namespace}-${var.stage}-db"
  database_name                   = "${var.namespace}${var.stage}db"
  password                        = "${var.rds_password}"
  engine                          = "${var.rds_engine}"
  engine_version                  = "${var.rds_engine_version}"
  vpc_id                          = "${module.vpc.vpc_id}"
  subnets                         = "${module.subnets.private_subnet_ids}"
  replica_count                   = 1
  instance_type                   = "${var.rds_instance_class}"
  storage_encrypted               = "${var.rds_storage_encrypted}"
  apply_immediately               = true
  skip_final_snapshot             = true
  db_parameter_group_name         = aws_db_parameter_group.aurora_db_postgres125_parameter_group.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.aurora_cluster_postgres125_parameter_group.id
  tags                            = "${var.tags}"
}

resource "aws_db_parameter_group" "aurora_db_postgres125_parameter_group" {
  name        = "prd-aurora-db-postgres125-parameter-group"
  family      = "aurora-postgresql12"
  description = "prd-aurora-db-postgres125-parameter-group"
}

resource "aws_rds_cluster_parameter_group" "aurora_cluster_postgres125_parameter_group" {
  name        = "prd-aurora-postgres125-cluster-parameter-group"
  family      = "aurora-postgresql12"
  description = "prd-aurora-postgres125-cluster-parameter-group"
}

# ////////////////////////////////////////////////////////////////////////////////////////////
# RDS security group
# ////////////////////////////////////////////////////////////////////////////////////////////

resource "aws_security_group" "app_servers" {
  name_prefix = "app-servers-"
  description = "For application servers"
  vpc_id      = "${module.vpc.vpc_id}"
}

resource "aws_security_group_rule" "allow_access" {
  type                     = "ingress"
  from_port                = module.db.this_rds_cluster_port
  to_port                  = module.db.this_rds_cluster_port
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.app_servers.id
  security_group_id        = module.db.this_security_group_id
}