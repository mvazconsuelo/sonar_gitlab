variable "namespace" {
  type        = string
  default     = "gitlab"
  description = "Namespace, which could be your organization name, e.g. 'eg' or 'cp'"
}

variable "stage" {
  type        = string
  default     = "prd"
  description = "Stage, e.g. 'prod', 'staging', 'dev' or 'testing'"
}

variable "name" {
  type        = string
  default     = "gitlab"
  description = "Solution name, e.g. 'app' or 'cluster'"
}

variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `name`, `namespace`, `stage`, etc."
}

variable "attributes" {
  type        = list
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = map
  default     = {
    Owner = "xxx"
    Team = "DevOps"
    }
  description = "Additional tags (e.g. `map('BusinessUnit`,`XYZ`)"
}


variable "allowed_security_groups_cluster" {
  type        = list
  default     = []
  description = "List of Security Group IDs to be allowed to connect to the EKS cluster"
}

variable "allowed_security_groups_workers" {
  type        = list
  default     = []
  description = "List of Security Group IDs to be allowed to connect to the worker nodes"
}

variable "allowed_cidr_blocks_cluster" {
  type        = list
  default     = []
  description = "List of CIDR blocks to be allowed to connect to the EKS cluster"
}

variable "allowed_cidr_blocks_workers" {
  type        = list
  default     = []
  description = "List of CIDR blocks to be allowed to connect to the worker nodes"
}

variable "region" {
  type        = string
  default     = "us-east-1"
  description = "AWS Region"
}

variable "vpc_cidr_block" {
  type        = string
  default     = "10.212.0.0/20"
  description = "VPC CIDR block. See https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html for more details"
}

variable "image_id" {
  type        = string
  default     = ""
  description = "EC2 image ID to launch. If not provided, the module will lookup the most recent EKS AMI. See https://docs.aws.amazon.com/eks/latest/userguide/eks-optimized-ami.html for more details on EKS-optimized images"
}

variable "eks_worker_ami_name_filter" {
  type        = string
  description = "AMI name filter to lookup the most recent EKS AMI if `image_id` is not provided"
  default     = "amazon-eks-node-1.18-v*"
}

variable "kubernetes_version" {
  type        = string
  default     = "1.18"
  description = "Desired Kubernetes master version. If you do not specify a value, the latest available version is used"
}

variable "oidc_provider_enabled" {
  type        = bool
  default     = true
  description = "Create an IAM OIDC identity provider for the cluster, then you can create IAM roles to associate with a service account in the cluster, instead of using kiam or kube2iam. For more information, see https://docs.aws.amazon.com/eks/latest/userguide/enable-iam-roles-for-service-accounts.html"
}


variable "health_check_type" {
  type        = string
  description = "Controls how health checking is done. Valid values are `EC2` or `ELB`"
  default     = "EC2"
}

variable "max_size" {
  default     = 4
  description = "The maximum size of the AutoScaling Group"
}

variable "min_size" {
  default     = 2
  description = "The minimum size of the AutoScaling Group"
}

variable "wait_for_capacity_timeout" {
  type        = string
  description = "A maximum duration that Terraform should wait for ASG instances to be healthy before timing out. Setting this to '0' causes Terraform to skip all Capacity Waiting behavior"
  default     = "30m"
}

variable "associate_public_ip_address" {
  description = "Associate a public IP address with the worker nodes in the VPC"
  default     = false
}

variable "autoscaling_policies_enabled" {
  type        = string
  default     = "true"
  description = "Whether to create `aws_autoscaling_policy` and `aws_cloudwatch_metric_alarm` resources to control Auto Scaling"
}

variable "cpu_utilization_high_threshold_percent" {
  type        = string
  default     = "80"
  description = "Worker nodes AutoScaling Group CPU utilization high threshold percent"
}

variable "cpu_utilization_low_threshold_percent" {
  type        = string
  default     = "20"
  description = "Worker nodes AutoScaling Group CPU utilization low threshold percent"
}

variable "availability_zones" {
  type        = list
  default     = ["us-east-1a", "us-east-1b","us-east-1c"]
  description = "Availability Zones for the cluster"
}


# EKS
# Nodes
variable "instance_type" {
  description = "AWS EC2 Instance type for Worker Nodes in Auto Scaling Group"
  default = "m5a.xlarge"
}
# Master
variable "eks_version" {
  description = "AWS EKS version"
  default = "1.18"
}


variable "environment" {
  description = "Environment to deploy. (e.g. `prod` `staging` `testing` `develop` `develop`)"
  default = "prd"
}
variable "team" {
  description = "Team owner of the Application (e.g. `backend` `frontend` `billing`)"
  default = "DevOps"
}
variable "owner" {
  description = "Creator of the infraestructure."
  default = "deploy"
}

variable "enabled" {
  type        = string
  description = "Whether to create the resources. Set to `false` to prevent the module from creating any resources"
  default     = "true"
}

variable "asg_desired_capacity" {
  type        = string
  description = "Desired capacity for eks autoscaling group"
  default     = "2"
}

variable "asg_max_size" {
  type        = string
  description = "Max Size for eks autoscaling group"
  default     = "4"
}

variable "asg_min_size" {
  type        = string
  description = "Min Size for eks autoscaling group"
  default     = "2"
}
variable "apply_config_map_aws_auth" {
  type        = string
  default     = "true"
  description = "Whether to generate local files from `kubeconfig` and `config_map_aws_auth` and perform `kubectl apply` to apply the ConfigMap to allow the worker nodes to join the EKS cluster"
}



# RDS Postgresql

variable "rds_db_parameter_group" {
  type        = string
  default     = "postgres12"
  description = "Parameter group, depends on DB engine used"
}

variable "rds_engine" {
  type        = string
  default     = "aurora-postgresql"
  description = "Engine for RDS"
}
variable "rds_engine_version" {
  type        = string
  default     = "12.5"
  description = "Engine version for RDS"
}
variable "rds_instance_class" {
  type        = string
  default     = "db.r4.large"
  description = "Instance class for RDS"
}
variable "rds_family" {
  type        = string
  default     = "postgres12"
  description = "The family of the DB parameter group"
}
variable "rds_major_engine_version" {
  type        = string
  default     = "12.5"
  description = "Specifies the major version of the engine that this option group should be associated with"
}
variable "rds_port" {
  type        = string
  default     = "5432"
  description = "Port for RDS instance"
}
variable "rds_username" {
  type        = string
  default     = "postgres"
  description = "Username for RDS"
}
variable "rds_password" {
  type        = string
  default     = "xxx"
  description = "Password for RDS"
}
variable "rds_maintenance_window" {
  type        = string
  default     = "Mon:00:00-Mon:03:00"
  description = "Maintenance window for RDS"
}
variable "rds_backup_window" {
  type        = string
  default     = "03:00-06:00"
  description = "Backup window for RDS Backups"
}
variable "rds_allocated_storage" { 
  type        = string
  default     = "200"
  description = "The allocated storage in gigabytes"
}
variable "rds_storage_encrypted" {
  type        = string
  default     = "false"
  description = "Specifies whether the DB instance is encrypted"
}
variable "rds_deletion_protection" {
  type        = string
  default     = "false"
  description = "The database can't be deleted when this value is set to true."
}

variable "rds_backup_retention_period" {
  type        = string
  default     = "0"
  description = "The days to retain backups for. Disable backups to create DB faster with 0 value"
}